import { User } from './models/User';
import { UserEdit } from './views/UserEdit';
import { Collection } from './Collection';
import { UserList } from './views/UserList';

const root = document.getElementById('root');

if( root ) {
	const userEdit = new UserEdit(root, User.buildUser({ name: 'Titi', age: 25 }));
	userEdit.render();
	console.log(userEdit);
} else {
	throw new Error('Root element not found');
}


const usersListView = document.getElementById('usersListView');
const users = new Collection('http://localhost:3000/users', User.buildUser);
users.fetch();

users.on('change', () => {
	if( usersListView ) {
		const usersList = new UserList(usersListView, users);
		usersList.render();
		console.log(users);
	}
});