import { Callback } from '../Eventing';


// ToDo: Why not use cb in curse?
interface ModelForView {
	on(eventName: string, cb: Callback): void
}


abstract class View<T extends ModelForView> {
	regions: { [key: string]: Element } = {};

	constructor(public parent: Element, public model: T) {
		this.bindModel();
	}

	regionsMap(): { [key: string]: string } {

		return {};
	}

	bindModel() {
		this.model.on('change', () => {
			this.render();
		});

	}

	bindEvents(fragment: DocumentFragment): void {
		const eventsMap = this.eventsMap();

		for( let eventKey in eventsMap ) {
			const [eventName, selector] = eventKey.split(':');

			fragment.querySelectorAll(selector).forEach(e => {
				e.addEventListener(eventName, eventsMap[eventKey]);
			});
		}
	}

	mapRegions(fragment: DocumentFragment): void {
		const regionsMap = this.regionsMap();

		for( let key in regionsMap ) {
			const selector = regionsMap[key];
			const e = fragment.querySelector(selector);

			if( e ) {
				this.regions[key] = e;

			}
		}
	}

	abstract template(): string

	eventsMap(): { [key: string]: () => void } {
		return {};
	}

	onRender(): void { }

	render(): void {
		this.parent.innerHTML = '';
		const templateElement = document.createElement('template');
		templateElement.innerHTML = this.template();
		this.bindEvents(templateElement.content);

		this.mapRegions(templateElement.content);

		this.onRender();

		this.parent.append(templateElement.content);
	}
}


export { View };