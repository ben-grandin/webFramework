import { View } from './View';
import { User } from '../models/User';


class UserForm extends View<User> {

	eventsMap(): { [key: string]: () => void } {
		return {
			'click:.set-age': this.onSetAgeClick,
			'click:.set-name': this.onSetNameClick,
			'click:.save-model': this.onSaveClick
		};
	}

	onSaveClick = (): void => {
		this.model.save();
	};
	onSetAgeClick = (): void => {
		this.model.setRandomAge();
		console.log(this.model.get('age'));
	};

	onSetNameClick = (): void => {
		const input = this.parent.querySelector('input');

		if( input ) {
			const { value } = input;
			this.model.set({ name: value });
		}
	};

	template(): string {
		return `
		<div>
			<input placeholder="${this.model.get(('name'))}"/>
			 <button class="set-name">Update name</button>
			<button class="set-age">Set random age</button>
			<button class="save-model">Save model</button>
		</div>
		`;
	}


}


export { UserForm };