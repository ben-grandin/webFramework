import { View } from './View';
import { User } from '../models/User';


class UserShow extends View<User> {

	template(): string {
		return `
		<div>
			<h3>User Detail</h3>
			<div>User name: ${this.model.get('name')}</div>
			<div>User age: ${this.model.get('age')}</div>
		</div>
		`;
	}

}


export { UserShow };